export const QUALITY_MIN = 0.05;
export const QUALITY_MAX = 1;
export const QUALITY_STEP = 0.05;
const MIME_TYPES = {
  JPEG: { value: "image/jpeg", name: "jpeg" },
  PNG: { value: "image/png", name: "png" },
  WEBP: { value: "image/webp", name: "webp" }
};

export default MIME_TYPES;
