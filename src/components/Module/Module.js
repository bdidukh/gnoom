import React from "react";
import "./Module.css";
import { QualityRange } from "../QualityRange/QualityRange";

export class Module extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showWindow: false
    };
    this.showModule = this.showModule.bind(this);
  }
  renderContent() {
    const { quality, OriginalSRC, CompressedSRC } = this.props;
    return (
      <div className="module">
        <div className="module_inner">
          <div className="origFoto">
            <img alt={"Gnoom"} src={OriginalSRC} className="module__image" />
            <img
              alt={"Gnoom"}
              src={CompressedSRC}
              className={"module__image-compressed module__image"}
            />
            <button onClick={this.showModule} className="closeButton">
              <i onClick={this.onClick} className="material-icons" id="button">
                close
              </i>
            </button>
            <QualityRange
              quality={quality}
              onQualityChange={this.props.onQualityChange}
            />
          </div>
        </div>
      </div>
    );
  }

  showModule() {
    this.setState({
      showWindow: !this.state.showWindow
    });
    window.gtag("event", "open module");
  }
  render() {
    return (
      <div className="app">
        <button onClick={this.showModule} className="NeBu">
          <i className="material-icons">crop</i>
        </button>
        {this.state.showWindow && this.renderContent()}
      </div>
    );
  }
}
