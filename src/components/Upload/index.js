import React from "react";
import "./Upload.css";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = { draggable: "", style: "" };
    this.onDragOver = this.onDragOver.bind(this);
    this.onDragLeave = this.onDragLeave.bind(this);
  }
  onDragOver() {
    this.setState({
      draggable: (this.drag.draggable = true)
    });
  }
  onDragLeave() {
    this.setState({ draggable: (this.drag.draggable = false) });
  }
  onInputChange = event => {
    let items = [];
    for (let key = 0; key < event.target.files.length; key++) {
      if (key < 10 && event.target.files[key].size < 50000000) {
        items.push(event.target.files[key]);
      }
      window.gtag("event", "on load");
    }
    this.props.onChange(items);
  };
  renderLabel() {
    return (
      <div
        className={`Upload ${this.state.draggable ? "Upload-dragging" : ""}`}
        ref={drag => {
          this.drag = drag;
        }}
        onDragOver={this.onDragOver}
        onDragLeave={this.onDragLeave}
        onDrop={this.onDrop}
      >
        <i id="center" className="material-icons">
          add_a_photo
        </i>
        <input
          id="menu_images"
          type="file"
          name="image"
          multiple
          accept="image/*"
          onChange={this.onInputChange}
        />
        <span className="text">
          {this.state.draggable ? "Drop Your Photo!" : "Choose Your Photo!"}
        </span>
      </div>
    );
  }

  render() {
    return <div>{this.renderLabel()}</div>;
  }
}
