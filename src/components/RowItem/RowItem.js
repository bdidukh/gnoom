import React from "react";
import { Module } from "../Module/Module";
import Compressor from "compressorjs";

import "./RowItem.css";
import { MimeType } from "../MimeType/MimeType";

export default class RowItem extends React.Component {
  state = {
    src: "",
    size: "",
    quality: null,
    valueType: ""
  };

  componentDidMount() {
    if (this.props.file.type === "image/jpeg") {
      this.updateCompressor();
    } else {
      this.sendToServer();
    }
    this.setState({ size: (this.props.file.size / 1000).toFixed(2) });
    let reader = new FileReader();
    reader.onload = e => {
      this.setState({ src: e.target.result });
    };

    if (this.props.file) {
      reader.readAsDataURL(this.props.file);
      let url = URL.createObjectURL(this.props.file);
      this.setState({ OriginalSRC: url });
    }
  }

  get quality() {
    return this.state.quality || this.props.quality;
  }

  sendToServer() {
    const formData = new FormData();
    const { file } = this.props;
    formData.append("picture", file, file.filename);
    formData.append("quality", this.quality);
    fetch("http://localhost:3005/upload", {
      method: "POST",
      body: formData
    })
      .then(function(responce) {
        if (responce.ok) {
          return responce.json();
        }
        throw new Error("Network responce that ");
      })
      .then(data => {
        this.setState({
          CompressedSRC: data.source,
          compressedSize: data.size / 1000
        });
      })
      .catch(function(error) {
        console.log("There is a problem " + error.message);
      });
  }

  updateCompressor = () => {
    new Compressor(this.props.file, {
      quality: this.quality,
      success: this.onCompress,
      mimeType: this.state.valueType,
      error(err) {
        console.log(err.message);
      }
    });
  };

  onCompress = result => {
    this.setState({ compressedSize: result.size / 1000 });
    let url = URL.createObjectURL(result);
    this.setState({ CompressedSRC: url });
  };

  onQualityChange = quality => {
    this.setState({ quality: quality });
    this.updateCompressor();
  };

  dropDownChange = valueType => {
    this.setState({ valueType: valueType });
    setTimeout(() => {
      if (valueType === "image/png") {
        this.sendToServer();
      } else {
        this.updateCompressor();
      }
    });
  };

  onClick = () => {
    window.gtag("event", "photoClick", { method: "Google" });
  };
  render() {
    const {
      src,
      size,
      compressedSize,
      CompressedSRC,
      OriginalSRC,
      valueType
    } = this.state;
    const {
      file: { type }
    } = this.props;
    return (
      <tr className="pictures__item">
        <td>
          <img
            onClick={this.onClick}
            alt={"Gnoom"}
            src={src}
            className="pictures__image"
          />
        </td>

        <td className="dataPict">{size}Kb</td>
        <td className="dataPict">{compressedSize}Kb</td>
        <td>
          <MimeType
            typeFile={type}
            valueType={valueType}
            dropDownChange={this.dropDownChange}
          />
        </td>
        <td>
          <Module
            OriginalSRC={OriginalSRC}
            quality={this.quality}
            onQualityChange={this.onQualityChange}
            CompressedSRC={CompressedSRC}
          />
        </td>
        <td>
          {CompressedSRC && (
            <a
              className="downLoad material-icons"
              href={CompressedSRC}
              download
            >
              cloud_download
            </a>
          )}
        </td>
      </tr>
    );
  }
}
