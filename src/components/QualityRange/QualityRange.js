import React from "react";
import "./QualityRange.css";
import * as constant from "../../constants";

export class QualityRange extends React.Component {
  state = {
    value: this.props.quality
  };

  onChange = event => {
    this.setState({ value: parseFloat(event.target.value) });
    setTimeout(() => {
      if (this.state.value !== this.props.quality) {
        this.props.onQualityChange(this.state.value);
      }
    }, 150);
    window.gtag("event", "quality", {
      quality: event.target.value
    });
    this.props.onQualityChange(parseFloat(event.target.value));
  };

  render() {
    const { quality } = this.props;
    return (
      <div className="moduleRange">
        <input
          className="quality-range"
          type="range"
          id="ranger"
          min={constant.QUALITY_MIN}
          max={constant.QUALITY_MAX}
          step={constant.QUALITY_STEP}
          onChange={this.onChange}
          value={quality}
        />
        <p className="qualityValue1">{quality}</p>
      </div>
    );
  }
}
