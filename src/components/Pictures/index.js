import React from "react";
import RowItem from "../RowItem/RowItem";
import "./Pictures.css";
export default class Pictures extends React.Component {
  render() {
    const items = [];

    for (let key = 0; key < this.props.pictures.length; key++) {
      items.push(
        <RowItem
          key={this.props.pictures[key].size}
          file={this.props.pictures[key]}
          quality={this.props.quality}
        />
      );
    }

    if (this.props.pictures.length) {
      return (
        <table className="pictures">
          <tbody>
            <tr className="pictures__item">
              <th />
              <th>Original Size</th>
              <th>Compressed Size</th>
            </tr>
            {items}
          </tbody>
        </table>
      );
    }

    return <div />;
  }
}
