import React from "react";
import MIME_TYPES from "../../constants";
import "./MimeType.css";

export class MimeType extends React.Component {
  changeFile() {
    const { typeFile, valueType } = this.props;
    if (
      typeFile === MIME_TYPES.JPEG.value ||
      typeFile === MIME_TYPES.WEBP.value
    ) {
      return (
        <div>
          <select
            onChange={event => this.props.dropDownChange(event.target.value)}
            value={valueType}
            className="select_type"
          >
            <option value={MIME_TYPES.JPEG.value}>
              {MIME_TYPES.JPEG.name}
            </option>
            <option value={MIME_TYPES.WEBP.value}>
              {MIME_TYPES.WEBP.name}
            </option>
          </select>
        </div>
      );
    }
    if (typeFile === MIME_TYPES.PNG.value) {
      return (
        <div>
          <select
            onChange={event => this.props.dropDownChange(event.target.value)}
            value={valueType}
          >
            <option value={MIME_TYPES.PNG.value}>{MIME_TYPES.PNG.name}</option>
            <option value={MIME_TYPES.JPEG.value}>
              {MIME_TYPES.JPEG.name}
            </option>
            <option value={MIME_TYPES.WEBP.value}>
              {MIME_TYPES.WEBP.name}
            </option>
          </select>
        </div>
      );
    }
  }

  render() {
    return <div>{this.changeFile()}</div>;
  }
}
