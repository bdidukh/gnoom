import React, { Component } from "react";
import "./App.css";
import Upload from "./components/Upload";
import Pictures from "./components/Pictures";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { pictures: [], draggable: "", quality: 0.6 };
    this.onDragOver = this.onDragOver.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.onDragLeave = this.onDragLeave.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onDragOver() {
    this.setState({ draggable: true });
  }
  onDrop = data => {
    this.setState({ pictures: data });
  };

  onChange = data => {
    window.gtag("event", "pictures", { data: data.length });
    this.setState({ pictures: data, draggable: false });
  };

  onDragLeave = () => this.setState({ draggable: false });

  onClick = () => {
    window.gtag("event", "logoClick");
  };

  render() {
    const { pictures, draggable } = this.state;
    return (
      <div
        className={`App ${this.state.draggable ? "App-dragging" : ""}`}
        ref={app => {
          this.app = app;
        }}
        onDragOver={this.onDragOver}
        onDragLeave={this.onDragLeave}
      >
        <main>
          <header className="App-header">
            <h1 onClick={this.onClick}>Gnoom</h1>
          </header>
          <Upload onChange={this.onChange} draggable={draggable} />
          <Pictures pictures={pictures} quality={this.state.quality} />
        </main>
      </div>
    );
  }
}

export default App;
