const express = require("express");
const multer = require("multer");
const sharp = require("sharp");
const app = express();
const upload = multer({ dest: "public/uploads/" });

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.listen(3005, function() {
  console.log("Example app listening on port 3005!\n");
});

app.get("/", function(req, res) {
  res.send("Hello world");
});

app.post("/upload", upload.single("picture"), async (req, res) => {
  const {
    file,
    body: { quality }
  } = req;
  if (file !== undefined && quality > 0 && quality <= 1) {
    if (file.mimetype === "image/png") {
      const data = await sharp(file.path)
        .png({ quality: quality * 100 })
        .toFile(`${file.path}.png`);
      res.send({ source: `/uploads/${file.filename}.png`, size: data.size });
    } else {
      throw new Error("error");
    }
  } else {
    throw new Error("Unavailable format");
  }
});
